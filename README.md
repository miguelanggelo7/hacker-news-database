# hacker-news-database



## Getting started

To run the project you need to clone the repo and install all dependences with npm i, then you are ready to test the proyect.

## Scripts

* npm run dev: It will be text de proyect executing nodemon and dotenv
* npm test: It will apply unit tests to the proyect
* npm test:coverage: It will apply tests coverage to the proyect

## Routes examples(by default page is 0)
* GET /articles?page=0
* GET /articles/byAuthor/:author?page=0
* GET /articles/byTitle/:title?page=0
* GET /articles/byTags/?tags=comment&tags=story&page=0
* DELETE /articles/byAuthor/:id

## Database

The database is automatically synchronized, you just need to set the configuration in database/db.js (host, database, username and pass)