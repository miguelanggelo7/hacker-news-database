const request = require("supertest");
const app = require("../src/app");

let testServer;


beforeAll(() => {
  testServer = app.listen(4000); 
});

afterAll((done) => {
  testServer.close(done);
});

describe("GET /articles", ( ) => {
  it("should return paginated articles 5 by 5", async () => {
    const response = await request(app).get("/articles");
    expect(response.error).toBe(false);
    expect(response.status).toBe(200);
    expect(response.body).not.toBeNull();
    expect(Array.isArray(response.body)).toBe(true);
    expect(response.body.length).toBeLessThanOrEqual(5);
  });
});

describe("GET /articles/byAuthor/:author", ( ) => {
  it("should return paginated articles filtered by author 5 by 5", async () => {
    const response = await request(app).get("/articles/byAuthor/trevcanhuman");
    expect(response.error).toBe(false);
    expect(response.status).toBe(200);
    expect(response.body).not.toBeNull();
    expect(Array.isArray(response.body)).toBe(true);
    expect(response.body.length).toBeLessThanOrEqual(5);
  });
});