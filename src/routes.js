const Router = require("express-promise-router");
const router = new Router();
const articles = require("./database/controllers/articles");

// Page query is default 0, that's the first page

// Get paginated items
router.get("/", async(req, res) => {
  const page = (typeof req.query.page === "undefined") ? 
    0 : parseInt(req.query.page) * 5;
  const data = await articles.getPaginatedAll(page);
  res.json(data)
});

// Get filtered items by Author
router.get("/byAuthor/:author", async(req, res) => {
  const author = req.params.author;
  const page = (typeof req.query.page === "undefined") ? 
    0 : parseInt(req.query.page) * 5;
  const data = await articles.getPaginatedByAuthor(page, author);
  res.json(data)
});

// Get filtered items by title matches
router.get("/byTitle/:title", async(req, res) => {
  const title = req.params.title;
  const page = (typeof req.query.page === "undefined") ? 
    0 : parseInt(req.query.page) * 5;
  const data = await articles.getPaginatedByTitle(page, title);
  res.json(data);
});

// Get filtered items by title matches
router.get("/byTags", async(req, res) => {
  const tags = req.query.tags;
  console.log(tags)
  const page = (typeof req.query.page === "undefined") ? 
    0 : parseInt(req.query.page) * 5;
  const data = await articles.getPaginatedByTags(page, tags);
  res.json(data);
});

// Delete article by id
router.delete("/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  await articles.delete(id);
  res.json({ message: `Article with id ${id} successfully removed`})
});


module.exports = router;