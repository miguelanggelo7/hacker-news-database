const express = require("express");
const axios = require("axios");
const app = express();
const routes = require("./routes");
const errorHandler = require("./errorHandler");

// Middlewares
app.use(express.json());

// Settings
app.set("port", process.env.PORT || 3000);

// Routing
app.use("/articles", routes);

// Error Handler
app.use(errorHandler);

module.exports = app;