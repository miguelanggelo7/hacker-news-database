const app = require("./app");
const db = require("./database/db");
const uploadData = require("./database/controllers/uploadData");

const setIntervalAndExecute = async () => {
  uploadData(db);
  return(setInterval(() => uploadData(), 1000 * 3600))
}

const main = () => {
  app.listen(app.get("port"), async () => {
    console.log(`Server listening on port ${app.get("port")}`)
    try {
      await db.authenticate();
      console.log("Succesfully connected to database");
    } catch (err) {
      console.error(err);
      return;
    }

    setIntervalAndExecute();
  });
};

main();