const errorHandler = (err, req, res, next) => {
  if(err === "cannot-delete") {
    return res.json({ message: "The article cannot be deleted" });
  }
  if (req.query.page < 0) {
    return res.json({ message: "ERROR you cannot put number less than 0 in page query"})
  };
}

module.exports = errorHandler;