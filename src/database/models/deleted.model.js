const { DataTypes } = require("sequelize");

module.exports = (db) => {
  const Deleted = db.define('deleted', {
    deletedId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    }
  }, {
    freezeTableName: true,
    timestamps: false
  });

  return Deleted; 
};