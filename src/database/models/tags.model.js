const { DataTypes } = require("sequelize");

module.exports = (db) => {
  const Tags = db.define('tags', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    tagName: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    indexes: [
      {
        unique: true,
        fields: ["tagName"]
      }
    ],
    freezeTableName: true,
    timestamps: false
  });
  
  return Tags;
};