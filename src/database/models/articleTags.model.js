module.exports = (db) => {
  const ArticleTags = db.define('articleTags', {}, {
    freezeTableName: true,
    timestamps: false
  });

  return ArticleTags;
};