const db = require("../db");
const { articles, tags, articleTags } = db.models;
const { Op, where } = require("sequelize");

const getPaginatedAll = async (offset) => {
  const data = await articles.findAll({
    order: [
      ["id", "ASC"]
    ],
    limit: 5,
    offset,
    raw: true
  });

  return data;
};

const getPaginatedByAuthor = async (offset, author) => {
  const data = await articles.findAll({
    where:{
      author,
    },
    order: [
      ["id", "ASC"]
    ],
    limit: 5,
    offset,
    raw: true
  });

  return data;
}

const getPaginatedByTitle = async (offset, title) => {
  const data = await articles.findAll({
    where: {
      title: {
        [Op.iLike]: `%${title}%`
      }
    },
    order: [
      ["id", "ASC"]
    ],
    limit: 5,
    offset,
    raw: true
  });

  return data;
};

const getPaginatedByTags   = async (offset, tagsArr) => {
  if (offset < 0) throw 'page cannot be less than 0';
  
  const data = await articles.findAll({
    distinct: true,
    include: [
      {
        model: articleTags,
        required: true,
        attributes: [],
        include: [
          {
            model: tags,
            required: true,
            attributes: [],
            where: {
              [Op.or]: 
                (typeof tagsArr === "undefined") 
                ? 
                []
                : 
                tagsArr.map(tag => (
                  {tagName: tag}
                )),
            }
          }
        ],
        
      },
    ],
    order: [
      ["id", "ASC"]
    ],
    raw: true
  });

  /*
    data is an array that have all matches at least one tag with tagsArr but we only want 
    the data that matches all tags 
  */
  const ocurr = {};

  data.forEach(item => {
    ocurr[item.id] = (typeof ocurr[item.id] === "undefined") ? 1 : ++ocurr[item.id];
  });

  const newData = data
    .filter(item => (typeof tagsArr === "undefined") ? true : ocurr[item.id] === tagsArr.length)
    .map(item => [item.id, item]);
  
  const nonDuplicates = [...new Map(newData).values()].splice(offset, 5);

  return nonDuplicates;
};

const deleteArticle = async (id) => {
  try {
    await db.transaction(async (t) => {
      const article = await articles.findByPk(id, {transaction: t});
      if (!article) throw "cannot-delete";
      await articles.destroy({ where: { id }, transaction: t });
      await articleTags.destroy({ where: { articleId: id }, transaction: t});
    });
  } catch (err) {
    throw "cannot-delete";
  }

};

module.exports = {
  getPaginatedAll,
  getPaginatedByAuthor,
  getPaginatedByTitle,
  getPaginatedByTags
};

module.exports.delete = deleteArticle;