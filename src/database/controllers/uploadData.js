const axios = require("axios");

const uploadData = async (db) => {
  const { articles, tags, articleTags, deleted } = db.models;
  const deletedArr = (await deleted.findAll({ raw: true })).map(item => item.deletedId);

  const response = await axios.get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs");
  const news = response.data.hits;
  
  for (const item of news) {
    /*
      The objectID of the story is the primary key of that
      because i realized that story_id is the same for some stories
      and there are some that do not have story_id
    */ 
  
    // I save the objectID as an integer id in the Articles table
    const data = {
      id: parseInt(item.objectID),
      title: item.story_title,
      author: item.author,
      url: item.story_url,
      story_id: item.story_id,
      created_at: item.created_at
    };

    // It is verified that it has not been previously deleted
    if (deletedArr.indexOf(data.id) === -1) {

      // If the article doesn't exists it will be created, otherwise it won't
      const [article, created] = await articles.findOrCreate({
        where: { 
          id: data.id,
        },
        defaults: {
          title: data.title,
          author: data.author,
          url: data.url,
          story_id: data.story_id,
          created_at: data.created_at,
        }
      });

      // If the article is just created their tags are also created
      if (created) {
        for (const tag of item._tags) {
          // If the tag doesn't exists it will be created, otherwise it won't
          const [tg, tgCreated] = await tags.findOrCreate({
            where: {
              tagName: tag
            },
          });

          await articleTags.findOrCreate({
            where: {
              articleId: data.id,
              tagId: tg.dataValues.id
            }
          });
        };
      };
    };

    
  };

};

module.exports = uploadData;