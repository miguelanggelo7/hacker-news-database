const { Sequelize } = require("sequelize");
const applyExtraSetup = require("./extra-setup");

const db = new Sequelize({
  dialect: "postgres",
  database: process.env.DB_DATABASE || "postgres",
  username: process.env.DB_USERNAME || "postgres",
  password: process.env.DB_PASS || "root",
  host: process.env.DB_HOST || "localhost"
});

require("./models/articles.model")(db);
require("./models/tags.model")(db);
require("./models/articleTags.model")(db);
require("./models/deleted.model")(db);

if (process.env.NODE_ENV !== "test") {
  applyExtraSetup(db);
};

module.exports =  db;