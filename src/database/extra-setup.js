const applyExtraSetup = async (db) => {
  const { articles, tags, articleTags, deleted } = db.models;
  articles.belongsToMany(tags, { through: articleTags  });
  tags.belongsToMany(articles, { through: articleTags });
  articles.hasMany(articleTags);
  articleTags.belongsTo(articles);
  tags.hasMany(articleTags);
  articleTags.belongsTo(tags);

  // Synchronizing models with the database
  await deleted.sync();
  await articles.sync();
  await tags.sync();
  await articleTags.sync();

  // Creating trigger that inserts data into deleted table
  await db.query(`
    CREATE OR REPLACE FUNCTION "insertDeletedData" () 
    RETURNS trigger AS $$
    BEGIN
      INSERT INTO deleted("deletedId") VALUES(OLD.id);
      RETURN OLD;
    END;
    $$ LANGUAGE plpgsql;

    DROP TRIGGER IF EXISTS "addDeleted" ON articles;

    CREATE TRIGGER "addDeleted"
    AFTER DELETE ON articles FOR EACH ROW
    EXECUTE PROCEDURE "insertDeletedData"();
  `);
};



module.exports = applyExtraSetup;